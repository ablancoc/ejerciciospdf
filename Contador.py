class Contador:
    def __init__(self, num=0):
        self.__numero = num
   
    def get_numero(self):
        return self.__numero

    def set_numero(self,val):
        self.__numero = val

    #Al devolver el self devuelves un contador y no un numero y asi puedes seguir haciendo operaciones sobre el(pe. print(contador.incrementar(1).decrementar(8)))
    def incrementar(self, incremento=1):                   
        self.__numero = self.__numero + incremento
        return self

    def decrementar(self, decremento=1):
        self.__numero = self.__numero - decremento
        return self

    def __str__(self):
        return str(self.__numero)

    def __eq__(self,other):
        return self.__numero == other.__numero

    def __ne__(self,other):
        return not self.__eq__(other)

contador = Contador(20)
print(contador.incrementar(1))
print(contador.decrementar(8))
print(contador)
print(contador.incrementar())
contador.set_numero(-1)
print(contador.get_numero())
