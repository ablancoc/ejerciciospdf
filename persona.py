# declaramos la clase persona
from glob import escape


class Persona:
    # declaramos el metodo __init__
    def __init__(self,n,e):
        self.nombre=n
        self.edad=e

    def get_nombre(self):
        return self.nombre

    def set_nombre(self, val):
        self.nombre = val
         
    # declaramos el metodo str. El +\ es para terminar la linea y saltar a la siguiente , donde se pone \n.
    def __str__(self):
        return "Nombre: " + self.nombre +\
               "\nEdad: " + str(self.edad)

# declaramos la clase empleado
# la clase empleado hereda los atributos y metodos de la clase Persona
class Empleado(Persona):
    # declaramos el metodo __init__
    def __init__(self,n,e,sueld):
        # llamamos al metodo init de la clase padre
        # utilizamos la funcion super() para hacer referencia al padre
        super().__init__(n,e)
        self.sueldo=sueld

    def __str__(self):
        return super().__str__() +\
               "\nSueldo: " + str(self.sueldo)

nomb=input("ingrese el nombre:")
ed=int(input("ingrese la edad:"))
 
p1=Persona(nomb,ed)
print(p1)

sueldo = float(input("Introduce el sueldo: "))
p2 = Empleado(nomb,ed,sueldo)
print(p2)
